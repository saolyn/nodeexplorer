function main() {
  plotTxs('eth')
    .then(() => plotAS('eth', 24))
    .then(() => plotAS('btc', 13))
    .then(() => plotNodes('eth'))
    .then(() => plotNodes('btc'));
}

const utils = {
  sum: (sum, val) => sum + val
};


function plotAS(currency, threshold) {
  return fetch(`data/${currency}-as.json`)
    .then(res => res.json())
    .then(jsonRes  => {
      let data = jsonRes.data;

      plotASBar(currency, data, threshold);

      let labels = Object.keys(data).map(as => data[as].as_name.length > 60 ? data[as].as_name.slice(0, 57) + '...' : data[as].as_name.slice(0, 97));
      let values = Object.keys(data).map(as => data[as].count);
      let popups = labels.map(label => Object.keys(data).filter(as => data[as].as_name === label).join('<br />'));

      let tracePie = {
        type: 'pie',
        values,
        labels,
        text: popups,
        textposition: 'inside',
        transforms: [{
          type: 'aggregate',
          groups: Object.keys(data).map(as => data[as].as_name),
          aggregations: [{ target: 'values', func: 'sum' }, ]
        },{
          type: 'sort',  target: 'values', order: 'ascending'
        }]
      };


      Plotly.newPlot(currency +'-as-pie', [tracePie]);
    })
    .catch(err => { throw err });
}

function plotASBar(currency, data, threshold) {
  const barData = {};

  // convert data to bar format
  Object.keys(data).forEach(as => {
    let asname = data[as].as_name;
    if (! barData.hasOwnProperty(asname)) {
      barData[asname] = { count: 0, asnumbers: []}
    }
    barData[asname].count += data[as].count;
    barData[asname].asnumbers.push(as);
  });

  // group the long tail based on threshold
  let tail  = {};
  if (threshold > 0) {
    Object.keys(barData).filter(asname => barData[asname].count <= threshold).forEach(asname => {
      tail[asname] = barData[asname];
    });
  }

  // insert tail back in bar data
  const tailLabel = `Nodes from ASs with < ${threshold + 1} nodes`;
  if (threshold > 0) {
    barData[tailLabel] = {
      count: Object.keys(tail).map(asname => barData[asname].count).reduce(utils.sum, 0),
      asnumbers: Object.keys(tail).map(asname => barData[asname].asnumbers).reduce((sum, value) => sum.concat(value), []),
    };
  }

  // remove data points which are in the tail
  if (threshold > 0) {
    Object.keys(barData).forEach(asname => {
      if (asname === tailLabel) { return; }
      if (barData[asname].count <= threshold) { delete  barData[asname]; }
    });
  }

  const labels = Object.keys(barData);
  const values = Object.keys(barData).map(as => barData[as].count);
  const colors = Object.keys(barData).map(asname => asname === tailLabel ? '#0496FF': '#0070C9');
  const popups = Object.keys(barData).map(asname => {
    const asnumbers = barData[asname].asnumbers;
    if (asname === tailLabel) {
      return `${asnumbers.length} ASs`;
    }
    return asnumbers.join('<br />');
  });
  const layout = {
    autosize: true,
    margin: { l: 300 },
    annotations: Object.keys(barData).map(asname => ({
      x: barData[asname].count + 16,
      y: asname,
      align: 'left',
      text: barData[asname].count,
      font: {size:12, color: '#000000'},
      showarrow: false
    }))
  };

  const trace = {
    type: 'bar',
    name: 'Nodes per Organization',
    orientation: 'h',
    x: values,
    y: labels,
    marker: { color: colors },
    text: popups,
    // textposition: 'outside',
    textfont: { color: '#EEF8FE'},
    transforms: [{
      type: 'sort',  target: 'x', order: 'ascending'
    }]
  };

  Plotly.newPlot(currency + '-as-bar', [trace], layout);
}

function plotNodes(currency) {
  return fetch(`data/${currency}-ver.json`)
    .then(res => res.json())
    .then(jsonRes  => {
      let data = jsonRes.data;
      let versionData;

      if (currency === 'eth') {
        versionData = parseEthNodesInfo(data);
      } else if (currency === 'btc') {
        versionData = parseBtcNodesInfo(data);
      }
      plotNodesPie(currency, versionData);
      plotDetailedVersions(currency, versionData);
    })
    .catch(err => { throw err });
}

function parseEthNodesInfo(versions) {
  const versionsData = {};
  Object.keys(versions).forEach(rawVersion => {
    let [node, version, platform, extra] = rawVersion.split('/', 4);
    if (node === 'geth-node-finder') {
      [version, extra] = [extra, version];
    }
    if (! versionsData.hasOwnProperty(node)) {
      versionsData[node] = []
    }
    versionsData[node].push({version, platform, extra, count: versions[rawVersion]})
  });
  return versionsData;
}

function parseBtcNodesInfo(versions) {
  const versionsData = {};
  Object.keys(versions).forEach(rawVersion => {
    let node, version, extra, protocol, nodeVersion, raw;
    [protocol, raw] = rawVersion.split(', ', 2);
    [nodeVersion, extra] = raw.split('/', 3).filter(x => x);
    if (nodeVersion === undefined) {
      node = 'N/A';
      version = 'N/A';
      extra = 'N/A';
    } else {
      [node, version] = nodeVersion.split(':', 2);
    }
    if (! versionsData.hasOwnProperty(node)) {
      versionsData[node] = []
    }
    versionsData[node].push({version, protocol, extra, count: versions[rawVersion]})
  });
  return versionsData;
}

function plotNodesPie(currency, data) {
  const trace = {
    type: 'pie',
    values: Object.keys(data)
      .map(node => data[node].map(info => info.count).reduce(utils.sum, 0)),
    labels: Object.keys(data),
    textposition: 'inside',
    transforms: [{ type: 'sort',  target: 'values', order: 'ascending' }]
  };
  Plotly.newPlot(currency +'-nodes-pie', [trace], {title: 'Shares of node implementations'});

}

function plotDetailedVersions(currency, data) {
  const layout = {
    autosize: true,
    margin: { l: 300 }
  };

  const nodeDiv = document.getElementById(currency + '-ver-bar');

  const sizes = Object.keys(data).map(node => (new Set(data[node].map(info => info.version))).size);
  const plots = sizes.length;
  const totalsize = sizes.reduce(utils.sum, 0) + (plots - 1) * 5;
  const step = 5 / totalsize

  Object.keys(data).forEach(node => {
    let versionTrace = {
      type: 'bar',
      name: node,
      orientation: 'h',
      y: data[node].map(info => info.version),
      x: data[node].map(info => info.count),
      transforms: [{
        type: 'aggregate',
        groups: data[node].map(info => info.version),
        aggregations: [{ target: 'x', func: 'sum' }, ]
      },{
        type: 'sort',  target: 'x', order: 'ascending'
      }]
    };
    layout.title = node;
    layout.height = 180 + 16 * (new Set(versionTrace.y)).size;

    let id = currency + '-ver-bar-' + node;
    let plotElem = document.createElement('div');
    plotElem.setAttribute('id', id);
    nodeDiv.appendChild(plotElem);
    Plotly.newPlot(id, [versionTrace], layout);
  });
}

function plotTxs(currency) {
  const nodeNames = {
    '41': 'Singapore',
    '1': 'Canada',
    '80': 'India'
  }
  return fetch(`data/${currency}-txs.json`)
    .then(res => res.json())
    .then(jsonRes  => {
      let data = jsonRes.data;
      console.log(data);

      const delays = {};
      Object.keys(data).forEach(tx => {
        for (let i = 0; i < data[tx].times.length - 1; i++) {
          let delay = data[tx].times[i + 1] - data[tx].times[i];
          let from  = data[tx].nodes[i];
          let to = data[tx].nodes[i + 1];
          if (delay < 0) {
            [from, to] = [to, from];
            delay = Math.abs(delay);
          }
          console.log('delay from ' + from+ ' to ' + to + ': ' + delay);

          if (!delays.hasOwnProperty(from)) { delays[from] = {}; }
          if (!delays[from].hasOwnProperty(to)) { delays[from][to] = []; }
          delays[from][to].push({tx, value: delay});
        }
      });
      console.log(delays);

      const traces = [];
      Object.keys(delays).forEach(from => {
        Object.keys(delays[from]).forEach(to => {
          traces.push({
            type: 'box',
            y: delays[from][to].map(delay => delay.value),
            name: `${nodeNames[from]} then ${nodeNames[to]}`,
            boxpoints: 'all',
            jitter: 0.3,
            pointpos: -1.8,
          })
        });
      });

      Plotly.newPlot('eth-txs-box', traces);
    })
    .catch(err => { throw err });
}

window.onload = main;