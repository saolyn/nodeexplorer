package data

import (
	"github.com/BurntSushi/toml"
	"fmt"
	"nodeExplorer/core"
	"nodeExplorer/database"
	"github.com/ammario/ipisp"
	"encoding/json"
	"io/ioutil"
	"strings"
)

func LoadASMap() map[string]string {
	var tmp struct { ASMap map[string]string }

	if _, err := toml.DecodeFile("./as_map.toml", &tmp); err != nil {
		panic(err)
	}
	return tmp.ASMap
}

var asmap = LoadASMap()

type AS struct {
	Data map[string]ASInfo `json:"data"`
}

type ASInfo struct {
	Count int       `json:"count"`
	OrgName string 	`json:"org_name"`
	ASName string   `json:"as_name"`
}

type Version struct {
	Data map[string]int `json:"data"`
}

type VersionInfo struct {
	Count int
	Platform string
}

type Transaction struct {
	Data map[string]TxObs `json:"data"`
}

type TxObs struct {
	Times []int64 `json:"times"`
	Nodes []int64 `json:"nodes"`
}


func ExportASData (currency string) {
	//Grab AS from db and check how many are reoccurring
	config := core.LoadConfig("./analysis_config.toml")
	database.Connect(config.Filepath)
	database.SelectASNumbers()
	ASNumbers, ASCount := database.SelectASNumbers()

	asData := AS{Data: make(map[string]ASInfo)}

	for i, ASNumber := range ASNumbers {
		ASName := GetAsName(ASNumber)
		asData.Data[ASNumber] = ASInfo{Count: ASCount[i], ASName: ASName}
	}

	asJSON, _ := json.Marshal(asData)
	filename := fmt.Sprintf("./app/data/%s-as.json", strings.ToLower(currency))
	if err := ioutil.WriteFile(filename, asJSON, 0444); err != nil {
		panic(err)
	}
	fmt.Printf("AS data exported to %s\n", filename)
}

func GetAsName (ASNumber string) string {
	if ASName, exists := asmap[ASNumber]; exists {
		return ASName
	}
	client, err := ipisp.NewWhoisClient()
	core.CheckErr("Could not connect to Whois client:", err)

	parsedASN, err := ipisp.ParseASN(ASNumber)
	core.CheckErr("Unable to parse ASN:", err)
	ASN, err := client.LookupASN(parsedASN)
	core.CheckErr("Unable to lookup ASN:", err)

	return ASN.Name.Short
}

func ExportVersionData (currency string) {
	config := core.LoadConfig("./analysis_config.toml")
	database.Connect(config.Filepath)
	versions, versionsCount := database.SelectNodeVersion()

	versionData := Version{Data: make(map[string]int)}
	for i, version := range versions{
		//Parse version
		versionData.Data[version] = versionsCount[i]
	}
	versionJSON, _ := json.Marshal(versionData)
	filename := fmt.Sprintf("./app/data/%s-ver.json", strings.ToLower(currency))
	if err := ioutil.WriteFile(filename, versionJSON, 0444); err != nil {
		panic(err)
	}
	fmt.Printf("Version data exported to %s\n", filename)
}

func ExportTransactionData (currency string) {
	config := core.LoadConfig("./analysis_config.toml")
	database.Connect(config.Filepath)
	hashes, timestamps, nodeIds := database.SelectTransactionData()

	txData := Transaction{Data: make(map[string]TxObs)}
	for i, hash := range hashes{
		//Parse version
		txData.Data[hash] = TxObs{Times:timestamps[i], Nodes:nodeIds[i]}
	}
	txJSON, _ := json.Marshal(txData)
	filename := fmt.Sprintf("./app/data/%s-txs.json", strings.ToLower(currency))
	if err := ioutil.WriteFile(filename, txJSON, 0444); err != nil {
		panic(err)
	}
	fmt.Printf("Version data exported to %s\n", filename)
}

