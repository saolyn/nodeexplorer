package normalize

import (
	"github.com/ethereum/go-ethereum/p2p"
	"net"
	"nodeExplorer/core"
	"nodeExplorer/database"
	"nodeExplorer/core/types"
)

func PeerFromEthereum(ethPeer *p2p.PeerInfo, timeObs int64, nodeID int) types.Peer {
	remoteAddress, _, err := net.SplitHostPort(ethPeer.Network.RemoteAddress)
	core.CheckErr("Failed to remove port: ", err)

	ASNumber := database.CheckExistingAS(remoteAddress)
	return types.Peer{ethPeer.Name, remoteAddress, ASNumber, timeObs, "ETH", nodeID}
}

func PeersFromEthereum (ethPeers []*p2p.PeerInfo, timeObs int64, nodeID int) []types.Peer {
	var peers []types.Peer
	for _, ethPeer := range ethPeers {
		peers = append(peers, PeerFromEthereum(ethPeer, timeObs, nodeID))
	}
	return peers
}

func TxFromEthereum (ethTx string, status core.Status, timeObs int64, nodeID int) types.Transaction {
	return types.Transaction{ethTx, timeObs, status, "ETH", nodeID}
}

func TxsFromEthereum (ethTxs []string, timeObs int64, nodeID int) []types.Transaction {
	var transactions []types.Transaction
	for _, ethTx := range ethTxs {
		transactions = append(transactions, TxFromEthereum(ethTx, core.Pending, timeObs, nodeID))
	}
	return transactions
}
