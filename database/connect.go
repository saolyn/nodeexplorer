package database

import (
	"database/sql"
	"fmt"
	"bytes"

	_ "github.com/lib/pq"

	"nodeExplorer/core"
	"nodeExplorer/core/types"
	"github.com/lib/pq"
)

var db *sql.DB

func Connect(filepath string) {
	var err error
	config := core.LoadConfig(filepath)
	ConnStr := fmt.Sprintf("postgres://%v:%v@%v/%v\n",
		config.Database.User, config.Database.Password, config.Database.Server, config.Database.DbName)
	fmt.Printf("Connecting to DB: %s\n", ConnStr)
	db, err = sql.Open("postgres", ConnStr)
	core.CheckErr("Could not connect to DB: ", err)
	fmt.Println("Successfully connected!")

	err = db.Ping()
	core.CheckErr("DB is unresponsive: ", err)
}

// UpdatePeers either updates previously inserted peers with setDiconnectionTime or inserts the currently connected
// peers with setConnectionTime.
func UpdatePeers (time int64, currency string, peers []types.Peer) {
	setDisconnectionTime(time, currency, peers)
	setConnectionTime(peers)
}

func InsertTransactions(pendingTxs []types.Transaction) {
	if len(pendingTxs) == 0 {
		return
	}
	buffer := bytes.NewBufferString("INSERT INTO transactions (hash, time_observed, current_status, currency, node_id) VALUES ")
	for i, tx := range pendingTxs {
		if i > 0 {
			buffer.WriteString(",")
		}
		buffer.WriteString(tx.TxAsSqlValue())
	}
	buffer.WriteString(" ON CONFLICT DO NOTHING")
	_, err := db.Exec(buffer.String())
	core.CheckErr("Could not insert transactions: ", err)
}

func UpdateTransaction(hash, currency string, status core.Status, time int64, nodeId int) {
	sqlStatement := `UPDATE transactions SET time_completed = $1, current_status = $2 where time_completed IS NULL AND hash = $3 AND currency = $4 AND node_id = $5`
	_, err := db.Exec(sqlStatement, time, status, hash, currency, nodeId)
	core.CheckErr("Could not update transactions: ", err)
}

// setDisconnectionTime iterates over the previously added peers to the database that do not have a diconnection
// time and compares them to the currently connected ones. If a peer is no longer in the list of connected peers
// but does not have a disconnection time then it updates the database setting the disconnection time observed.
func setDisconnectionTime(time int64, currency string, connectedPeers types.Peers) {
	buffer := bytes.NewBufferString("SELECT id FROM peers WHERE connection_end IS NULL AND NOT remote_address IN ('")
	for i, peer := range connectedPeers.GetAddresses() {
		if i > 0 {
			buffer.WriteString("','")
		}
		buffer.WriteString(peer)
	}
	buffer.WriteString("')")

	rows, err := db.Query(buffer.String())
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		err := rows.Scan(&id)
		core.CheckErr("No rows returned:", err)

		sqlStatement := `UPDATE peers SET connection_end = $1 WHERE currency = $2 AND connection_end IS NULL AND id = $3`
		_, err = db.Exec(sqlStatement, time, currency, id)
		core.CheckErr("Could not set disconnection times: ", err)
	}
}

// setConnectionTime inserts a list of the currently connected peers into the database.
// The peer information added is the peer name, remote address, ip range, as number, time the connection was
// observered, the currency associated to it and the node id of the node that observed it
func setConnectionTime(connectedPeers types.Peers) {
	if len(connectedPeers) == 0 {
		return
	}
	buffer := bytes.NewBufferString("INSERT INTO peers (name, remote_address, as_number, connection_start, currency, node_id) VALUES ")
	for i, peer := range connectedPeers {
		if i > 0 {
			buffer.WriteString(",")
		}
		buffer.WriteString(peer.PeerAsSqlValue())
	}
	buffer.WriteString(" ON CONFLICT DO NOTHING")
	_, err := db.Exec(buffer.String())
	core.CheckErr("Could not set connection times: ", err)
}

func CheckExistingAS(address string) string{
	sqlStatement := `SELECT as_number FROM peers WHERE remote_address = $1`
	row := db.QueryRow(sqlStatement, address)

	var ASNumber string
	err := row.Scan(&ASNumber)
	if err != nil {
		ASNumber := core.LookupRemoteAddress(address)
		return ASNumber
	}
	return ASNumber
}

func SelectASNumbers() ([]string, []int){
	var as_numbers	[]string
	var as_count	[]int
	rows, err := db.Query(`SELECT as_number, count(DISTINCT remote_address) FROM peers GROUP BY  as_number`)
	core.CheckErr("Failed to select as numbers:", err)
	defer rows.Close()

	var as_number string
	var count int
	for rows.Next() {
		err := rows.Scan(&as_number, &count)
		as_numbers = append(as_numbers, as_number)
		as_count = append(as_count, count)
		core.CheckErr("No rows returned:", err)
	}
	return as_numbers, as_count
}

func SelectNodeVersion() ([]string, []int){
	var names []string
	var nameCount []int
	rows, err := db.Query(`SELECT name, count(name) FROM peers GROUP BY name`)
	core.CheckErr("Failed to select peer name:", err)
	defer rows.Close()

	var name string
	var count int
	for rows.Next() {
		err := rows.Scan(&name, &count)
		names = append(names, name)
		nameCount = append(nameCount, count)

		fmt.Printf("Name: %s, count: %d\n", name, count)
		core.CheckErr("No rows returned:", err)
	}
	return names, nameCount
}

func SelectTransactionData() ([]string, [][]int64, [][]int64) {
	var hashes []string
	var timestamps [][]int64
	var nodeIds [][]int64
	rows, err := db.Query(`SELECT hash, array_agg(time_observed), array_agg(node_id) FROM transactions GROUP BY hash`)
	core.CheckErr("Failed to select peer name:", err)
	defer rows.Close()

	var hash string
	var timestamp pq.Int64Array
	var nodeId	pq.Int64Array
	for rows.Next() {
		err := rows.Scan(&hash, &timestamp, &nodeId)
		core.CheckErr("No rows returned:", err)

		if len(nodeId) < 2 {
			continue
		}

		hashes = append(hashes, hash)
		timestamps = append(timestamps, timestamp)
		nodeIds = append(nodeIds, nodeId)
	}
	return hashes, timestamps, nodeIds
}

// Disconnect closes the database connection
func Disconnect() {
	db.Close()
}