package node

import (
	"time"
	"fmt"
	"strconv"

	"nodeExplorer/database"
	"nodeExplorer/core"
	"nodeExplorer/core/types"
	"nodeExplorer/core/normalize"

	"github.com/btcsuite/btcd/rpcclient"
)

func BitcoinTxs() {
	var nextBlock int64 = 0

	config := core.LoadConfig("./bitcoin_config.toml")
	connCfg := &rpcclient.ConnConfig{
		Host:			config.ConnectRPC.Host,
		User: 			config.ConnectRPC.RpcUser,
		Pass:			config.ConnectRPC.RpcPassword,
		HTTPPostMode:	config.ConnectRPC.HTTPPostMode,
		DisableTLS:		config.ConnectRPC.DisableTLS,
	}
	client, err := rpcclient.New(connCfg, nil)
	core.CheckErr("Could not create rpc client: %v", err)
	defer client.Shutdown()

	database.Connect(config.Filepath)
	println("Connecting to client...")

	transactionTicker := time.NewTicker(config.Node.Ticker * time.Millisecond)
	blockTicker := time.NewTicker(15 * time.Second)
	quit := make(chan struct{})
	for {
		select {
		case <- transactionTicker.C:
			transactions, err := client.GetRawMempool()
			core.CheckErr("Could not fetch tx from mempool: %v", err)

			timeObs := time.Now().Unix()
			txs := normalize.TxsFromBitcoin(transactions, timeObs, config.Node.ID)
			database.InsertTransactions(txs)

		case <- blockTicker.C:
			latestBlockNumber, err := client.GetBlockCount()
			core.CheckErr("Could not fetch next block:", err)

			if nextBlock == 0 {
				fmt.Print("Getting latest block...")
				nextBlock = latestBlockNumber
			}
			for nextBlock <= latestBlockNumber {
				fmt.Printf("Getting block %d...", nextBlock)
				block := GetBTCBlock(client, nextBlock)
				fmt.Printf(" block %s, %d txs\n", block.Number, len(block.Transactions))

				timeComp, err := strconv.ParseInt(block.Timestamp, 0, 64)
				core.CheckErr("Can't convert timestamp:", err)
				for _, hash := range block.Transactions {
					database.UpdateTransaction(hash, config.Node.Currency, core.Success, timeComp, config.Node.ID)
				}
				nextBlock = block.GetNumber() + 1
			}

		case <- quit:
			transactionTicker.Stop()
			fmt.Println("Tickers stopped")
			database.Disconnect()
			return
		}
	}
}

func GetBTCBlock(client *rpcclient.Client, blockHeight int64) types.Block{
	hash, err := client.GetBlockHash(blockHeight)
	core.CheckErr(fmt.Sprintf("Can't get block hash for %d:", blockHeight), err)
	block, err := client.GetBlockVerbose(hash)
	blockData := normalize.BlockFromBitcoin(block)
	return blockData
}