package node

import (
	"time"
	"fmt"
	_ "database/sql"
	"github.com/ethereum/go-ethereum/rpc"
	"nodeExplorer/database"
	"nodeExplorer/core/types"
	"nodeExplorer/core"

	"strconv"
	"nodeExplorer/core/normalize"
)

var statusFromEth = map[string]core.Status {
	"0x1": core.Success,
	"0x0": core.Failed,
}

func normalizeStatus(ethStatus string) core.Status {
	return statusFromEth[ethStatus]
}

func EthereumTxs() {
	var transactionFilter string
	var transactionInfo *types.Receipt
	var nextBlock int64 = 0
	var block types.Block

	config := core.LoadConfig("./ethereum_tx_config.toml")
	database.Connect(config.Filepath)
	transactionTicker := time.NewTicker(config.Node.Ticker * time.Millisecond)
	blockTicker := time.NewTicker(15 * time.Second)
	quit := make(chan struct{})

	println("Connecting to Geth client...")
	client, err := rpc.Dial(config.ConnectRPC.Host)
	core.CheckErr("Could not create rpc client: %v", err)

	err = client.Call(&transactionFilter, "eth_newPendingTransactionFilter")
	core.CheckErr("Filter didn't start:", err)
	for {
		select {
		case <-transactionTicker.C:
			var transactions []string
			err = client.Call(&transactions, "eth_getFilterChanges", transactionFilter)
			core.CheckErr("Can't get list of transactions:", err)

			timeObs := time.Now().Unix()
			txs := normalize.TxsFromEthereum(transactions, timeObs, config.Node.ID)
			database.InsertTransactions(txs)

		case <-blockTicker.C:
			latestBlock := GetBlock(client, "latest")
			latestBlockNumber := latestBlock.GetNumber()

			fmt.Printf("LatestBlock: %d (%s)\n", latestBlockNumber, latestBlock.Number)

			if nextBlock == 0 {
				fmt.Print("Getting latest block...")
				nextBlock = latestBlockNumber
			}

			for nextBlock <= latestBlockNumber {
				fmt.Printf("Getting block %d...", nextBlock)
				block = GetBlock(client, fmt.Sprintf("0x%x", nextBlock))
				fmt.Printf(" block %s, %d txs\n", block.Number, len(block.Transactions))

				timeComp, err := strconv.ParseInt(block.Timestamp, 0, 64)
				core.CheckErr("Can't convert timestamp:", err)

				for _, hash := range block.Transactions {
					err = client.Call(&transactionInfo, "eth_getTransactionReceipt", hash)
					if err != nil {
						fmt.Printf("Can't get transaction receipt: %s\n", err)
						continue
					}
					status := normalizeStatus(transactionInfo.Status)
					database.UpdateTransaction(hash, config.Node.Currency, status, timeComp, config.Node.ID)
					fmt.Printf("Tx %s: status = %s\n", hash, status)
				}
				nextBlock = block.GetNumber() + 1
			}
			fmt.Println("Fetched all blocks")

		case <-quit:
			transactionTicker.Stop()
			fmt.Println("Tickers stopped")
			database.Disconnect()
			return
		}
	}
}

func GetBlock(client *rpc.Client, blockTag string) types.Block {
	var blockData types.Block
	err := client.Call(&blockData, "eth_getBlockByNumber", blockTag, false)
	core.CheckErr(fmt.Sprintf("Can't get block %s:", blockTag), err)
	return blockData
}